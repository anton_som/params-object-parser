window.addEventListener('DOMContentLoaded', () => {
    console.log('%c >>> DOM loaded ', 'background: #222; color: #bada55');

    let dataGenerated = false;

    let isFrame, areCaptionsOptional, isAddressOneliner;

    const inputArea = document.getElementById('input_area');
    const outputArea = document.getElementById('output_area');
    const copyButton = document.getElementById('copy_button');

    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (let i = 0; i < checkboxes.length; i++) {
            checkboxes[i].addEventListener('change', renewButtons);
            checkboxes[i].addEventListener('change', updateExample);
        }

    copyButton.addEventListener('click', () => {
        generateHTML();

        if (dataGenerated) {
            outputArea.select();
            document.execCommand('copy');
            copyButton.innerHTML = 'COPIED! <img class="button_icon" src="img/kitsoft-logo-white.svg"></img>';
            copyButton.classList.add('animate_width');
        }
    });

    function generateHTML() {
        outputArea.value = '';
        dataGenerated = false;

        if (inputArea.value == '') {
            riseWarning('no data');
            inputArea.select();
            renewButtons();
            return;
        }

        isFrame = (document.getElementById('frame_checkbox').checked) ? true : false;
        areCaptionsOptional = (document.getElementById('captions_checkbox').checked) ? true : false;
        isAddressOneliner = (document.getElementById('address_checkbox').checked) ? true : false;

        let wrapperBefore;
        if (isFrame) {
            wrapperBefore = `<div class='user-info-fop'><div class='user-info-fop-content'>`;
        } else {
            wrapperBefore = `<div style='width: 100%;'><div style='display: flex; flex-wrap: wrap;'>`;
        }
        const wrapperAfter = `</div></div>`;
        const captionStart = `<p style='width: 100%; font-size: 12px; line-height: 16px; opacity: 0.5; margin-bottom: 3px;'>`;
        const captionEnd = `</p>`;
        const textStart = `<p style='width: 100%; font-size: 18px; line-height: 28px; margin-bottom: 20px;'>`;
        const textEnd = `</p>`;

        let params;
        try {
            params = JSON.parse(inputArea.value.trim().replace(/^\"params\":/g, "").replace(/,$/g, ""));
        } catch (error) {
            riseWarning('is not valid JSON');
            return;
        }

        let paramsArray = Object.keys(params);

        if (isAddressOneliner) {

            let index = paramsArray.indexOf('region');
            if (index !== -1) {
                paramsArray[index] = 'addressOneliner';
            }
            paramsArray = paramsArray.filter(item => item !== 'index' && item !== 'region' && item !== 'district' && item !== 'locality' && item !== 'street' && item !== 'building' && item !== 'korpus' && item !== 'apt' );
        }

        let output = '';

        output += wrapperBefore;

        paramsArray.forEach(item => {

            if (isAddressOneliner && item === 'addressOneliner') {
                output += `  ${(areCaptionsOptional ? `{{#if region}}` : '')}${captionStart}\\f__Місцезнаходження/Адреса__${captionEnd}${(areCaptionsOptional ? `{{/if}}` : '')}`;
                output += `${textStart}{{#if index}}{{index}}, {{/if}}{{#if region}}{{region}}{{/if}}{{#if district}}, {{district}}{{/if}}{{#if locality}}, {{locality}}{{/if}}{{#if street}}, {{street}}{{/if}}{{#if building}}, буд.&nbsp;{{building}}{{/if}}{{#if korpus}}, корп.&nbsp;{{korpus}}{{/if}}{{#if apt}}, кв.&nbsp;{{apt}}{{/if}}${textEnd}  `;
            } else {
                output += `  ${(areCaptionsOptional ? `{{#if ${item}}}` : '')}${captionStart}\\f__${item.toUpperCase()}__${captionEnd}${(areCaptionsOptional ? `{{/if}}` : '')}`;
                output += `{{#if ${item}}}${textStart}{{${item}}}${textEnd}{{/if}}  `;
            }

        })

        output += wrapperAfter;

        outputArea.value = output;

        if (outputArea.scrollHeight > 400) {
            outputArea.style.height = '400px';
        } else {
            outputArea.style.height = 'auto';
            outputArea.style.height = outputArea.scrollHeight + 'px';
        }

        dataGenerated = true;
        console.log('%c <<< Data served ', 'background: #222; color: #bada55');
    }

    function updateExample() {
        console.log('update example');
        isFrame = (document.getElementById('frame_checkbox').checked) ? true : false;
        areCaptionsOptional = (document.getElementById('captions_checkbox').checked) ? true : false;
        isAddressOneliner = (document.getElementById('address_checkbox').checked) ? true : false;

        document.getElementById('example_image').src = `img/${isFrame ? 't' : 'f'}${areCaptionsOptional ? 't' : 'f'}${isAddressOneliner ? 't' : 'f'}.png`;
    }

    function renewButtons() {
        console.log('renew buttons');
        copyButton.classList.remove('animate_width');
        copyButton.textContent = 'GENERATE & COPY';
    }

    function riseWarning(type) {
        let warning;
        switch (type) {
            case 'is not valid JSON':
                warning = '← is not valid JSON';
                break;
            case 'no data':
                warning = '← NO DATA TO PARSE';
                break;
            default:
                console.log('%c Unidentified error', 'background: red; color: white');
                break;
        }
        console.log(`%c Warning: ${warning} `, 'background: red; color: white');
        outputArea.value = warning;
        outputArea.classList.add('error');
        dataGenerated = false;
        outputArea.style.height = 'auto';
        outputArea.style.height = outputArea.scrollHeight + 'px';
    }

    // Textarea autoresize by DreamTeK (https://stackoverflow.com/users/2120261/dreamtek)
    const tx = document.getElementsByTagName('textarea');
    for (let i = 0; i < tx.length; i++) {
        tx[i].setAttribute('style', 'height:' + (tx[i].scrollHeight) + 'px;');
        tx[i].addEventListener('input', OnInput);
        tx[i].addEventListener('change', OnInput);
    }

    function OnInput() {
        this.style.height = 'auto';
        if (this.scrollHeight > 400) {
            this.style.height = '400px';
        } else {
            this.style.height = (this.scrollHeight) + 'px';
        }

        renewButtons();
        
        if (!dataGenerated) {
            outputArea.value = '';
            outputArea.classList.remove('error');
        }
    }

});