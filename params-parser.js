// Params object parser

const isFrame = true; // Рамка: true — рамка-веселка, false — без рамки (для попап-htmlBlock)

const areCaptionsOptional = true; // Описи полів відображаються: true — коли поле існує, false — завжди

const isAddressOneliner = true; // true — формує адресу в один рядок, вставка відбувається у місці, де знаходиться поле `region` (поля адреси мають називатися index, region, district, locality, street, building, korpus, apt)

let wrapperBefore;

if (isFrame) {
    wrapperBefore = `<div class='user-info-fop'><div class='user-info-fop-content'>`;
} else {
    wrapperBefore = `<div style='width: 100%;'><div style='display: flex; flex-wrap: wrap;'>`;
}

const wrapperAfter = `</div></div>`;
const captionStart = `<p style='width: 100%; font-size: 12px; line-height: 16px; opacity: 0.5; margin-bottom: 3px;'>`;
const captionEnd = `</p>`;
const textStart = `<p style='width: 100%; font-size: 18px; line-height: 28px; margin-bottom: 20px;'>`;
const textEnd = `</p>`;

const params = {
    "regNumber": "regNumber",
    "issueDate": {
        "path": "issueDate",
        "transformVal": "val => moment(val).format('DD.MM.YYYY')"
    },
    "index": "location.ATU.building.index",
    "region": "location.ATU.region.name",
    "district": "location.ATU.district.name",
    "locality": "location.ATU.locality.name",
    "street": "location.street.stringified",
    "building": "location.building.building",
    "korpus": "location.building.korpus",
    "apt": "location.apt.apt",
    "ownerCompanyEdrpou": "calculated.userCompanyEdrpou",
    "ownerCompanyRegistrationDate": "calculated.externalReaderEdrEdrpou.0.registration.date",
    "userName": "calculated.userName",
    "phone": "calculated.userPhone"
};

let paramsArray = Object.keys(params);

if (isAddressOneliner) {

    let index = paramsArray.indexOf('region');
    if (index !== -1) {
        paramsArray[index] = 'addressOneliner';
    }
    paramsArray = paramsArray.filter(item => item !== 'index' && item !== 'region' && item !== 'district' && item !== 'locality' && item !== 'street' && item !== 'building' && item !== 'korpus' && item !== 'apt' );
}

console.log(paramsArray);

let output = '';

output += wrapperBefore;

paramsArray.forEach(item => {

    if (isAddressOneliner && item === 'addressOneliner') {

        output += `  ${(areCaptionsOptional ? `{{#if region}}` : '')}${captionStart}\\f__Місцезнаходження/Адреса__${captionEnd}${(areCaptionsOptional ? `{{/if}}` : '')}`;
        output += `${textStart}{{#if index}}{{index}}, {{/if}}{{#if region}}{{region}}{{/if}}{{#if district}}, {{district}}{{/if}}{{#if locality}}, {{locality}}{{/if}}{{#if street}}, {{street}}{{/if}}{{#if building}}, буд.&nbsp;{{building}}{{/if}}{{#if korpus}}, корп.&nbsp;{{korpus}}{{/if}}{{#if apt}}, кв.&nbsp;{{apt}}{{/if}}${textEnd}  `;

    } else {

        output += `  ${(areCaptionsOptional ? `{{#if ${item}}}` : '')}${captionStart}\\f__${item.toUpperCase()}__${captionEnd}${(areCaptionsOptional ? `{{/if}}` : '')}`;
        output += `{{#if ${item}}}${textStart}{{${item}}}${textEnd}{{/if}}  `;

    }

})

output += wrapperAfter;

console.log(`\n${output}\n`);

// v1.1 Anton Som, 2022.08
